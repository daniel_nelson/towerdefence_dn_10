﻿using UnityEngine;
using System.Collections;

public class PlayerRaycast : MonoBehaviour {

    RaycastHit hit;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.C))
        {
            print("C");
            
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            if (Physics.Raycast(ray, out hit, 7))
            {
                print("The Raycast is working");
                if (hit.collider.gameObject.CompareTag("Tower"))
                {
                    print("tower hit");
                    hit.collider.gameObject.GetComponent<TowerTracking>().SwitchTracking();
                }
            }
        }
    }
}
