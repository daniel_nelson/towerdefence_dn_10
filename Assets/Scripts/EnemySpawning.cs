﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

public class EnemySpawning : NetworkBehaviour
{

	public Vector3 spawnerPosition;
	private Vector3 spawnRandom;
	GameObject enemyClone;
	public GameObject basicEnemy;
	Grid grid;
	Pathfinder pathfinder;
	// Use this for initialization
	void Start()
	{
		spawnerPosition = GameObject.Find("SpawnZone").transform.position;
	}
	Vector3 UpdateSpawnPos(Vector3 centreSpawnPos, float range){
		Vector3 spawnPos;
		spawnPos.x = centreSpawnPos.x + (Random.Range (-range, range));
		spawnPos.z = centreSpawnPos.z + (Random.Range (-range, range));
		spawnPos.y = centreSpawnPos.y;
		return spawnPos;
	}
	// Update is called once per frame
	void Update()
	{
		if (GameObject.FindGameObjectsWithTag ("Player").Length > 0) {
			if (Input.GetKeyDown (KeyCode.F)) {
				Debug.Log("You pressed it - monster coming");
				EnemyType playerHunter = new EnemyType ("Player Hunter", BasicEnemy.targetStateEnum.targetPlayer, basicEnemy, 1);
				SpawnEnemy (playerHunter, spawnerPosition, 4f);
			}
			if (Input.GetKeyDown (KeyCode.T)) {
				EnemyType playerHunter = new EnemyType ("Player Hunter", BasicEnemy.targetStateEnum.targetPlayer, basicEnemy);
				SpawnEnemy (playerHunter, spawnerPosition, 4f);
			}
		}
	}
	public void SpawnEnemy(EnemyType enemy, Vector3 spawnPos, float spawnRadius){
		enemyClone = (GameObject)Instantiate (enemy.prefab, UpdateSpawnPos (spawnPos, spawnRadius), Quaternion.identity);
		NetworkServer.Spawn (enemyClone);
		enemyClone.GetComponent<BasicEnemy>().setAttributes(5, 10, 1, 5, BasicEnemy.targetStateEnum.targetPlayer);


	}
}
